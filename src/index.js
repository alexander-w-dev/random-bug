require('./style.css');
const { RandomBug } = require('./classes/random-bug.class');

function drawRandomBug(targetId) {
  const targetElem = document.getElementById(targetId);
  targetElem.innerHTML = '';

  const randomBug = new RandomBug({
    width: 200,
    height: 200,
  });
  targetElem.appendChild(randomBug.getZonesHtmlNode());
}

const drawRandomBugButton = document.getElementById('draw-random-bug-button');
drawRandomBugButton.onclick = () => drawRandomBug('bug');

drawRandomBug('bug');
