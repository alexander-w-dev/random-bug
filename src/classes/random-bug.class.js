'use strict';
import _ from 'lodash';
import { ZoneFactory } from './helpers/zone-factory';
import { HtmlFactory } from './helpers/html-factory';

// version 2.0
export class RandomBug {
  /**
   * 
   * @param { IInputOptions } options 
   */
  constructor(options) {
    /** @type { IOptions } */
    this.options = Object.assign({}, this.getDefaultOptions(), options);
    this.options.halfWidth = Math.floor(this.options.width / 2);

    /** @type { IZones } */
    this.zones = ZoneFactory.generateRandomZones(this.options);
    
    this.bugMatrix = this.getEmptyArrayBySizes(this.options.height, this.options.halfWidth);
  }


  getZonesHtmlNode(){
    return HtmlFactory.getZoneHtml(this.zones, this.options);
  }

  getEmptyArrayBySizes(height, width) {
    var arr = [];
    for (var i = 0; i < height; i++){
      arr[i] = [];
      for (var j = 0; j < width; j++){
        arr[i][j] = 0;
    }}

    return arr;
  }

  /** @returns { IInputOptions } */
  getDefaultOptions() {
    return {
      width: 10,
      height: 10,
    }
  }

}