export class HtmlFactory {}

/**
 * @param { IZones } zones 
 * @param { IOptions } options 
 */
HtmlFactory.getZoneHtml = (zones, options) => {
  const result = document.createElement('div');
  result.style.display = 'flex';

  const rightSide = document.createElement('div');
  const leftSide = document.createElement('div');
  result.appendChild(leftSide);
  result.appendChild(rightSide);

  rightSide.style.width = leftSide.style.width = options.halfWidth + 'px';
  rightSide.style.height = leftSide.style.height = options.height + 'px';
  rightSide.style.position = leftSide.style.position = 'relative';

  Object.keys(zones).forEach(partName => {
    const part = zones[partName];

    const leftSidePartNode = document.createElement('div');
    leftSide.appendChild(leftSidePartNode);
    const rightSidePartNode = document.createElement('div');
    rightSide.appendChild(rightSidePartNode);
    leftSidePartNode.style.backgroundColor = rightSidePartNode.style.backgroundColor = part.zoneColor;
    leftSidePartNode.style.width = rightSidePartNode.style.width = part.width.pix + 'px';
    leftSidePartNode.style.height = rightSidePartNode.style.height = part.height.pix + 'px';
    leftSidePartNode.style.position = rightSidePartNode.style.position = 'absolute';
    leftSidePartNode.style.top = rightSidePartNode.style.top = part.position.top + 'px';
    // mirror position
    leftSidePartNode.style.right = rightSidePartNode.style.left = part.position.left + 'px';
  });

  return result;
}