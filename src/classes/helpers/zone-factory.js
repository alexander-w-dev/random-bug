'use strict';

import randomInt from 'random-int';
import arrayShuffle from 'array-shuffle';

const MAX_MIN__PROCENT_SIZES = {
  mustache: {
    width: {
      proc: { min: 30, max: 90 }
    },
    height: {
      proc: { min: 20, max: 50 }
    }
  },
  head: {
    width: {
      proc: { min: 10, max: 30 }
    },
    height: {
      proc: { min: 10, max: 20 }
    }
  },
  body: {
    width: {
      proc: { min: 20, max: 40 }
    },
    height: {
      proc: { min: 20, max: 40 }
    }
  },
  tail: {
    width: {
      proc: { min: 20, max: 40 }
    },
    height: {
      proc: { min: 30, max: 60 }
    }
  },
  paw: {
    width: {
      proc: { min: 30, max: 80 }
    },
    height: {
      proc: { min: 30, max: 90 }
    }
  }
}

export class ZoneFactory{
  /** 
   * @param { IOption } options
   * @return { IZones } */
  static generateRandomZones(options) {
    return (new ZoneFactory(options)).getZones();
  }

  /**
   * @param { IOptions } options 
   */
  constructor(options) {
    /** @type { IOptions } */
    this.options = options;

    /** @type { IZones } */
    this.zones = this.getEmptyZones();

    // staep 1:
    this.generateRandomProcentSizes();
    
    // step 2:
    this.generatePixelSizes();
    
    // step 3:
    this.generatePositions();
  }

  /** @public */
  getZones() {
    return this.zones;
  }

  /** 
   * @private
   * @return { IZones }
   */
  getEmptyZones() {
    return {
      mustache: {
        name: 'mustache',
        zoneColor: 'rgba(245, 0, 0, 0.46)',
        width: {
          proc: 0,
          pix: 0,
        },
        height: {
          proc: 0,
          pix: 0,
        },
        position: {
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }
      },
      head: {
        name: 'head',
        zoneColor: 'rgba(0, 114, 255, 0.56)',
        width: {
          proc: 0,
          pix: 0,
        },
        height: {
          proc: 0,
          pix: 0,
        },
        position: {
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }
      },
      body: {
        name: 'body',
        zoneColor: 'rgba(255, 165, 0, 0.5)',
        width: {
          proc: 0,
          pix: 0,
        },
        height: {
          proc: 0,
          pix: 0,
        },
        position: {
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }
      },
      tail: {
        name: 'tail',
        zoneColor: 'rgba(0, 128, 0, 0.45)',
        width: {
          proc: 0,
          pix: 0,
        },
        height: {
          proc: 0,
          pix: 0,
        },
        position: {
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }
      },
      paw: {
        name: 'paw',
        zoneColor: 'rgba(79, 78, 78, 0.42)',
        width: {
          proc: 0,
          pix: 0,
        },
        height: {
          proc: 0,
          pix: 0,
        },
        position: {
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }
      },
    }
  }

  /** @private */
  generatePositions() {
    const OFFSET_INDEX = .25;

    this.zones.mustache.position.top = 0;
    this.zones.mustache.position.bottom = this.zones.mustache.height.pix;
    this.zones.mustache.position.left = 0;
    this.zones.mustache.position.right = this.zones.mustache.width.pix;

    this.zones.head.position.top = randomInt(
      this.zones.mustache.position.bottom - Math.min(Math.round(this.zones.mustache.height.pix * OFFSET_INDEX), this.zones.head.height.pix),
      this.zones.mustache.position.bottom
    );
    this.zones.head.position.bottom = this.zones.head.position.top + this.zones.head.height.pix;
    this.zones.head.position.left = 0;
    this.zones.head.position.right = this.zones.head.width.pix;

    this.zones.body.position.top = Math.max(
      randomInt(
        this.zones.head.position.bottom - Math.min(Math.round(this.zones.head.height.pix * OFFSET_INDEX), this.zones.body.height.pix),
        this.zones.head.position.bottom
      ),
      this.zones.mustache.position.bottom
    );
    this.zones.body.position.bottom = this.zones.body.position.top + this.zones.body.height.pix;
    this.zones.body.position.left = 0;
    this.zones.body.position.right = this.zones.body.width.pix;

    this.zones.tail.position.top = Math.max(
      randomInt(
        this.zones.body.position.bottom - Math.min(Math.round(this.zones.body.height.pix * OFFSET_INDEX), this.zones.tail.height.pix),
        this.zones.body.position.bottom
      ),
      this.zones.head.position.bottom
    );
    this.zones.body.position.bottom;
    this.zones.tail.position.bottom = this.zones.body.position.bottom + this.zones.tail.height.pix;
    this.zones.tail.position.left = 0;
    this.zones.tail.position.right = this.zones.tail.width.pix;

    const pawLeft = randomInt(
      this.zones.body.position.right - Math.round(this.zones.body.width.pix * OFFSET_INDEX) - Math.min(Math.round(this.zones.body.position.right * OFFSET_INDEX), this.zones.tail.width.pix),
      this.zones.body.position.right - Math.round(this.zones.body.width.pix * OFFSET_INDEX)
    );
    this.zones.paw.position.top = this.zones.body.position.top;
    this.zones.paw.position.bottom = this.zones.head.position.bottom + this.zones.paw.height.pix;
    this.zones.paw.position.left = pawLeft;
    this.zones.paw.position.right = pawLeft + this.zones.paw.width.pix;
  }

  /** @private */
  generatePixelSizes() {
    this.zones
    Object.keys(this.zones).forEach(partName => {
      this.zones[partName].height.pix = Math.round(this.zones[partName].height.proc / 100 * this.options.height);
      this.zones[partName].width.pix = Math.round(this.zones[partName].width.proc / 100 * this.options.halfWidth);
    });
  }

  /** @private */
  generateRandomProcentSizes() {
    this.devideSizesBetween(['head.height.proc', 'body.height.proc', 'tail.height.proc', 'mustache.height.proc']);
    this.devideSizesBetween(['body.width.proc', 'paw.width.proc']);
    this.devideSizesBetween(['mustache.width.proc']);
    this.zones.tail.width.proc = this.zones.body.width.proc;


    this.zones.head.width.proc = randomInt(
      Math.min(_.get(MAX_MIN__PROCENT_SIZES, 'head.width.proc').min, this.zones.body.width.proc), 
      Math.min(_.get(MAX_MIN__PROCENT_SIZES, 'head.width.proc').max, this.zones.body.width.proc)
    )

    this.zones.paw.height.proc = randomInt(
      Math.min(_.get(MAX_MIN__PROCENT_SIZES, 'paw.height.proc').min, 100 - this.zones.mustache.height.proc - this.zones.head.height.proc), 
      Math.min(_.get(MAX_MIN__PROCENT_SIZES, 'paw.height.proc').max, 100 - this.zones.mustache.height.proc - this.zones.head.height.proc)
    )
  }

  /** @private */
  devideSizesBetween(arr) {
    let shuffledArr = arrayShuffle(arr);
    let procTotal = 100;
    shuffledArr.forEach(shuffledItem => {
      const coefficient = procTotal / 100;
      const min = _.get(MAX_MIN__PROCENT_SIZES, shuffledItem).min * coefficient;
      const max = _.get(MAX_MIN__PROCENT_SIZES, shuffledItem).max * coefficient;
      const proc = randomInt(min, max);
      _.set(this.zones, shuffledItem, proc);
      procTotal -= proc;
    });
  }


}