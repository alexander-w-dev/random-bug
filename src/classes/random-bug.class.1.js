export class RandomBug {
  constructor(options) {
    this.options = Object.assign({}, this.getDefaultOptions(), options);

    this.bugMatrix = this.generateRandomBugMatrix();
  }

  getDefaultOptions() {
    return {
      width: 10,
      height: 10,
    }
  }

  randomInterval(start, end) {
    const a = end - start;
    if (a <= 1 || a >= 0) {
      return start + Math.round(Math.random() * a);
    } else if (a > 1) {
      return start + Math.floor(Math.random() * a) + 1;
    } else {
      throw new Error('end less start');
    }
  }
  
  getFilledLine(lineLength, pointCounts, previousLine) {
    if (lineLength < pointCounts) throw new Error('lineLength can not to be less than pointCounts');

    const line = [];
    const emptyLineIndexes = [];
    for (let j = 0; j < lineLength; j++) {
        line.push(0);
        emptyLineIndexes.push(j);
    }

    let infinitWhileControl = 0;
    let maxWhileIterations = lineLength * 100
    while (pointCounts) {
      if (infinitWhileControl++ > maxWhileIterations) break; //throw new Error('Too many while iterations');
      const indexOfEmptyLineIndex = this.randomInterval(0, emptyLineIndexes.length - 1)
      const randomIndex = emptyLineIndexes[indexOfEmptyLineIndex];
      if (line[randomIndex] === 0) {
        if (previousLine) {
          if (previousLine[randomIndex] === 1 || previousLine[randomIndex - 1] === 1 || previousLine[randomIndex + 1] === 1) {
            pointCounts--;
            emptyLineIndexes.splice(indexOfEmptyLineIndex, 1);
            line[randomIndex] = 1;
          }
        } else {
          pointCounts--;
          emptyLineIndexes.splice(indexOfEmptyLineIndex, 1);
          line[randomIndex] = 1;
        }
      }
      
    }

    return line;
  }

  generateRandomBugMatrix() {
    const width = this.options.width;
    const height = this.options.height;
  
    if (width % 2) throw new Error('Width should devition on 2!');
  
    const maxInterval = 2;
    const halfWidth = width / 2;
  
    const verticalPointCounts = [];

    for (let i = 0; i < halfWidth; i++) {
      verticalPointCounts.push(this.randomInterval(0, height));
    }
    verticalPointCounts.sort((a, b) => b - a);
  
    let previousLine;
    const horizontarHalfBug = [];
    for (let i = 0; i < halfWidth; i++) {
        const line = this.getFilledLine(height, verticalPointCounts[i], previousLine);
        previousLine = line;
        horizontarHalfBug.push(line);
    }
  
    const verticalHalfBug = [];
    for (let i = 0; i < height; i++) {
      const line = [];
      for (let j = 0; j < halfWidth; j++) {
          line.push(horizontarHalfBug[j][i])
      }
      verticalHalfBug.push(line);
    }
    
    const result = [];
    const verticalBug = verticalHalfBug.map(verticalHalfBugLine => {
      return [].concat(verticalHalfBugLine).reverse().concat(verticalHalfBugLine);
    });
  
    return verticalBug;
  }

  getHtmlNode() {
    const result = document.createElement('div');
    result.classList.add('bug');
  
    this.bugMatrix.forEach(bugMatrixLine => {
      const divLine = document.createElement('div');
      divLine.classList.add('bug-line');
      result.appendChild(divLine);
      bugMatrixLine.forEach(point => {
          const divPoint = document.createElement('div');
          divPoint.classList.add('bug-point');
          if (point) {
              divPoint.classList.add('filled');
          }
          divLine.appendChild(divPoint);
      });
    });

    return result;
  } 

}