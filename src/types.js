
/**
 * @typedef IZone
 * @type { object }
 * @property { string } name 
 * @property { string } zoneColor 
 * @property { { proc: number, pix: number } } width
 * @property { { proc: number, pix: number } } height
 * @property { { top: number, bottom: number, left: number, right: number } } position
 */

/**
 * @typedef IZones
 * @type { object }
 * @property { IZone } mustache 
 * @property { IZone } head 
 * @property { IZone } body 
 * @property { IZone } tail 
 * @property { IZone } paw
 */

 /**
  * @typedef IInputOptions
  * @type { object }
  * @property { number } width
  * @property { number } height
 */

 /**
  * @typedef IOptions
  * @type { object }
  * @property { number } halfWidth
  * @property { number } width
  * @property { number } height
  */
